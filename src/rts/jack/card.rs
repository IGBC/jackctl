use crate::model::events::{JackCardAction, JackCardHandle};
use crate::rts::jack::JackRuntime;
use jack::Client;
use std::sync::Arc;

#[derive(Debug)]
enum IODirection {
    In,
    Out,
}

pub async fn spawn_handle(jack: Arc<JackRuntime>) {
    // Loop until the card_tx senders drop
    while let Ok(card) = jack.card_rx.recv().await {
        debug!("Handling jack card event...");
        match card {
            (
                JackCardAction::StartCard {
                    id,
                    name,
                    in_ports,
                    out_ports,
                    rate,
                },
                r,
            ) => {
                let result = launch_card(
                    &jack.a_client.as_client(),
                    &id,
                    &name,
                    rate,
                    in_ports,
                    out_ports,
                    jack.n_periods,
                    jack.resample_q,
                );
                r.reply(result).await.unwrap();
            }
            (JackCardAction::StopCard { id }, r) => {
                stop_card(&jack.a_client.as_client(), id.0);
                stop_card(&jack.a_client.as_client(), id.1);
                r.reply(id).await.unwrap();
            }
        }
    }
}

#[cfg(feature = "zalsa")]
fn configure_client(
    id: &str,
    direction: &IODirection,
    rate: u32,
    psize: u32,
    nperiods: u32,
    quality: u32,
    channels: u32,
) -> (&'static str, String) {
    let args = format!(
        "-d hw:{} -r {} -p {} -n {} -c {}",
        id, rate, psize, nperiods, channels
    );
    let client = match direction {
        IODirection::In => "zalsa_in",
        IODirection::Out => "zalsa_out",
    };
    (client, args)
}

#[cfg(not(feature = "zalsa"))]
fn configure_client(
    id: &str,
    direction: &IODirection,
    rate: u32,
    psize: u32,
    nperiods: u32,
    quality: u32,
    channels: u32,
) -> (&'static str, String) {
    let client = "audioadapter";
    let args = match direction {
        IODirection::In => format!(
            "-d hw:{} -r {} -p {} -n {} -q {} -i {}",
            id, rate, psize, nperiods, quality, channels
        ),
        IODirection::Out => format!(
            "-d hw:{} -r {} -p {} -n {} -q {} -o {}",
            id, rate, psize, nperiods, quality, channels
        ),
    };
    (client, args)
}

fn start_client(
    client: &Client,
    id: &str,
    direction: IODirection,
    name: &str,
    rate: u32,
    psize: u32,
    nperiods: u32,
    quality: u32,
    channels: u32,
) -> Result<jack::InternalClientID, jack::Error> {
    let (c_name, args) = configure_client(id, &direction, rate, psize, nperiods, quality, channels);
    let p_name = match direction {
        IODirection::In => format!("{} - Input", name),
        IODirection::Out => format!("{} - Output", name),
    };
    info!("Running {} with: {}", c_name, args);
    info!("jack_load \"{}\" {} -i \"{}\"", p_name, c_name, args);
    client.load_internal_client(&p_name, &c_name, &args)
}

fn launch_card(
    client: &Client,
    id: &str,
    name: &str,
    rate: u32,
    in_ports: u32,
    out_ports: u32,
    nperiods: u32,
    quality: u32,
) -> JackCardHandle {
    let psize = client.buffer_size();
    let client_in = if in_ports > 0 {
        start_client(
            client,
            id,
            IODirection::In,
            name,
            rate,
            psize,
            nperiods,
            quality,
            in_ports,
        )
        .map_err(|e| {
            error!(
                "Card input {} ({}) Could not be started by jack: {}",
                id, name, e
            )
        })
        .ok()
    } else {
        None
    };

    let client_out = if out_ports > 0 {
        start_client(
            client,
            id,
            IODirection::Out,
            name,
            rate,
            psize,
            nperiods,
            quality,
            out_ports,
        )
        .map_err(|e| {
            error!(
                "Card output {} ({}) Could not be started by jack: {}",
                id, name, e
            )
        })
        .ok()
    } else {
        None
    };

    (client_in, client_out)
}

fn stop_card(client: &Client, id: Option<jack::InternalClientID>) {
    if id.is_some() {
        let result = client.unload_internal_client(id.unwrap());
        if result.is_err() {
            crate::log::oops(format!("Failed to stop client: {}", result.unwrap_err()), 1);
        }
    }
}
